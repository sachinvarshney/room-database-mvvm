package com.example.jetpackdemo

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.jetpackdemo.adapter.UserRecyclerAdapter
import com.example.jetpackdemo.database.User
import com.example.jetpackdemo.databinding.ActivityUserListBinding
import com.example.jetpackdemo.listener.ItemClickLIstener
import com.example.jetpackdemo.viewModel.UserVIewModel

class UserListActivity : AppCompatActivity(), ItemClickLIstener {
    lateinit var binding: ActivityUserListBinding
    lateinit var userVIewModel: UserVIewModel
    lateinit var userRecyclerAdapter: UserRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_list)
        val userVMinstance: UserVIewModel by viewModels()
        userVIewModel = userVMinstance

        userVIewModel.userList.observe(this, androidx.lifecycle.Observer { userList ->
            if (userList != null && userList.size > 0) {
                setAdapter(userList)
                setVisibility(true)
            } else {
                setVisibility(false)
            }
        })
    }

    private fun setAdapter(userList: List<User>) {
        binding.recyclerview.layoutManager = LinearLayoutManager(this)
        binding.recyclerview.setHasFixedSize(true)
        userRecyclerAdapter =
            UserRecyclerAdapter(userList as ArrayList<User>, this)
        binding.recyclerview.adapter = userRecyclerAdapter

    }

    private fun setVisibility(isDataAvaialbe: Boolean) {
        if (isDataAvaialbe) {
            binding.recyclerview.visibility = View.VISIBLE
            binding.noData.visibility = View.GONE

        } else {
            binding.recyclerview.visibility = View.GONE
            binding.noData.visibility = View.VISIBLE
        }
    }

    fun deleteItem(position: Int) {
        userVIewModel.deletetUser(position)
    }

    override fun onItemClick(position: Int) {
        deleteItem(position)
    }

}