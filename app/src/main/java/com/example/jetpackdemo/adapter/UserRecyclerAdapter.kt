package com.example.jetpackdemo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.jetpackdemo.R
import com.example.jetpackdemo.database.User
import com.example.jetpackdemo.databinding.ListItemBinding
import com.example.jetpackdemo.listener.ItemClickLIstener

class UserRecyclerAdapter(
    val userList: ArrayList<User>,
    val itemClickLIstener: ItemClickLIstener
) :
    RecyclerView.Adapter<UserRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: ListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item, parent, false
        )
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(userList.get(position), position)
    }

    inner class MyViewHolder(val binding: ListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(user: User, position: Int) {
            binding.user = user
            binding.holder = this
            binding.position = position
            binding.executePendingBindings()
        }

        fun onItemClick(view: View, position: Int) {

            if (view.id == R.id.delete) {
                itemClickLIstener.onItemClick(position)
            }

        }

    }
}