package com.example.jetpackdemo

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.jetpackdemo.databinding.ActivityDemoViewModelBinding
import com.example.jetpackdemo.viewModel.CounterViewModel

class DemoViewModelActivity : AppCompatActivity() {
    lateinit var binding: ActivityDemoViewModelBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_demo_view_model)
        val counterViewModel: CounterViewModel by viewModels()
        binding.counterViewModel = counterViewModel
        binding.setLifecycleOwner(this)

       /* counterViewModel.count.observe(this, androidx.lifecycle.Observer { count ->
            binding.count.text = count.toString()
        })

        binding.increment.setOnClickListener({

            counterViewModel.incrementCount()
//            binding.count.text = counterViewModel.count.toString()
        })
        binding.decrement.setOnClickListener({
            counterViewModel.decrementCount()
//            binding.count.text = counterViewModel.count.toString()
        })*/

    }
}