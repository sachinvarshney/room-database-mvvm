package com.example.jetpackdemo.viewModel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.jetpackdemo.Repository.UserRepository
import com.example.jetpackdemo.database.User

class UserVIewModel(application: Application) : AndroidViewModel(application) {
    var context: Context
    var userRepository: UserRepository
    var userList: LiveData<List<User>>

    init {
        context = getApplication<Application>().applicationContext
        userRepository = UserRepository(context)
        userList = userRepository.getAllUserList()

    }

    fun insertUser(user: User) {
        userRepository.insertUser(user)
    }

    fun deletetUser(position: Int) {
        userList.value?.get(position)?.let { userRepository.deletetUser(it) }

    }

    fun getUserInfo(name: String) {
        userRepository.getUserInfo(name)
    }

}