package com.example.jetpackdemo.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class CounterViewModel : ViewModel() {
    var count: MutableLiveData<Int> = MutableLiveData()

    init {
        count.value = 0;
    }

    fun incrementCount() {
        count.value = count.value?.plus(1)
    }

    fun decrementCount() {
        count.value = count.value?.minus(1)
    }

}