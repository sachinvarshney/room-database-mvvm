package com.example.jetpackdemo

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.jetpackdemo.database.User
import com.example.jetpackdemo.databinding.ActivityMainBinding
import com.example.jetpackdemo.viewModel.UserVIewModel

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    lateinit var userVIewModel: UserVIewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        val userVMinstance: UserVIewModel by viewModels()
        userVIewModel = userVMinstance
    }

    fun onSubmitClick(view: View) {
        if (TextUtils.isEmpty(binding.nameEt.text)) {
            binding.nameTil.error = getString(R.string.warning_name)
            return
        }

        if (TextUtils.isEmpty(binding.emailEt.text)) {
            binding.emailTil.error = getString(R.string.warning_email)
            return
        }

        userVIewModel.insertUser(
            User(binding.nameEt.text.toString(), binding.emailEt.text.toString())
        )
        startActivity(Intent(this, UserListActivity::class.java))
    }

    fun showList(view: View) {
        startActivity(Intent(this, UserListActivity::class.java))
    }

    fun viewModelDemo(view: View) {
        startActivity(Intent(this, DemoViewModelActivity::class.java))
    }

}