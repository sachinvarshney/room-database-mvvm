package com.example.jetpackdemo.Repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.jetpackdemo.database.AppDatabase
import com.example.jetpackdemo.database.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserRepository(context: Context) {
    var database: AppDatabase? = null
    var userList: LiveData<List<User>>

    init {
        database = AppDatabase.getInstance(context)
        userList = database?.userDao()?.getAll()!!
    }


    fun insertUser(user: User) {
        CoroutineScope(Dispatchers.IO).launch {
            database?.userDao()?.insertAll(user)
        }
    }

    fun deletetUser(user: User) {
        CoroutineScope(Dispatchers.IO).launch {
            database?.userDao()?.delete(user)
        }
    }

    fun getUserInfo(name: String): User {
        return database?.userDao()?.getUserInfo(name)!!
    }

    fun getAllUserList(): LiveData<List<User>> {
        return userList
    }

}